#!/bin/bash

BASE_DIR="/cephfs-miniflax-kernel/testattr"
NJOBS=20
NDIRS=10
NFILES=10

function print_help() {
    echo "Usage: $0 [options]"
    echo " --basedir -b base directory to run jobs on (default: $BASE_DIR)"
    echo " --njobs,  -n  Number of parallel jobs (default: $NJOBS)"
    echo " --ndirs,  -d  Number of directories to create (default: $NDIRS)"
    echo " --nfiles, -f  Number of files per directory (default: $NFILES)"
    echo " --help,   -h  Display this help message"
    exit 0
}

OPT_SPEC=":b:n:d:f:h"
while getopts "$OPT_SPEC" opt; do
    case $opt in
        b) # Base directory (--basedir)
            BASE_DIR="$OPTARG"
            ;;
        n) # Number of parallel jobs (--njobs)
            NJOBS="$OPTARG"
            ;;
        d)  # Number of directroes (--ndirs)
            NDIRS="$OPTARG"
            ;;
        f) # files per dir (--nfiles)
            NFILES="$OPTARG"
            ;;
        :)
            echo "Option -$OPTARG requires an argument"
            exit 1
            ;;
        h)
            print_help
            ;;
        \?)
            echo "Invalid Option: -$OPTARG"
            exit 1
            ;;

    esac
done

shift $((OPTIND-1))
# Check for remaining invalid arguments
if [[ $# -gt 0 ]]; then
    echo "Invalid arguments provided."
    print_help
fi

echo $BASE_DIR

test -d $BASE_DIR || (echo "No $BASE_DIR found" && exit 1)

dir_seq=$(seq 1 $NDIRS)
file_seq=$(seq 1 $NFILES)

parallel mkdir -p $BASE_DIR/dir{} ::: "$dir_seq"
export BASE_DIR
parallel --env BASE_DIR -j $NJOBS 'tmp_f=$(mktemp -p "$BASE_DIR"/dir{1}/ tmp.XXXXXXXXXX); exec {myfd}> $tmp_f; sleep 1; echo testdata>&${myfd};exec {myfd}>&- ; mv $tmp_f "$BASE_DIR"/dir{1}/file{2}' ::: "$dir_seq" ::: "$file_seq"
