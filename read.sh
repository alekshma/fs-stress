i#!/bin/bash
set -x
trap "echo Exit on Ctrl-C; exit;" SIGINT SIGTERM

basedir="${1:-/cephfs-miniflax-kernel/testattr}"

while true;
do
    time find $basedir -type f -exec stat {} > /dev/null \; ;
done
